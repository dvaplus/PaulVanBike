declare module 'errio' {
    const value: any
    export default value
}
declare module 'ErrorUtils' {
    const value: any
    export default value
}
declare module 'mobx-react/native' {
    const value: any
    export default value
    export const observer: any
}
declare module 'raven-js' {
    const value: any
    export default value
}
declare module 'react-native-code-push' {
    const value: any
    export default value
}
declare module 'react-native-easy-grid' {
    const value: any
    export default value
    export const
        Grid: any,
        Col: any,
        Row: any
}
declare module 'react-native-extended-stylesheet' {
    const value: any
    export default value
}
declare module 'react-native-lightbox' {
    const value: any
    export default value
}
declare module 'simple-react-mobx-router' {
    const value: any
    export default value
}
declare module 'react-native-vector-icons/Foundation' {
    const value: any
    export default value
}
declare module 'react-native-vector-icons/Ionicons' {
    const value: any
    export default value
}
declare module 'react-native-view-transformer' {
    const value: any
    export default value
}

declare module 'react-native-button' {
    import {Component} from 'react'
    import RN from 'react-native'
    interface Props {
        style?: RN.ViewStyle
        styleDisabled?: RN.ViewStyle
        onPress?: () => any
    }
    export default class Button extends Component<Props, any> {}
}
declare module 'react-timer-mixin' {
    const value: any
    export default value
}

declare module '*.json' {
    const value: any
    export default value
}
